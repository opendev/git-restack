.. include:: ../../CONTRIBUTING.rst

Running tests
=============
To run git-restack tests the following commands may by run::

    tox -e py27
    tox -e py26
    tox -e py32
    tox -e py33

depending on what Python interpreter would you like to use.
