=======
 Usage
=======

To interactively rebase the current branch against the most recent
commit in common with the master branch, run::

    git restack

If your branch is based on a different branch, run::

    git restack branchname
