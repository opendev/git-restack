============
 git-restack
============

``git-restack`` is a tool that helps edit a series of commits without
rebasing.

.. toctree::
   :maxdepth: 2

   installation
   usage
   developing


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

