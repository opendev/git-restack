================================
 Installation and Configuration
================================

Installing git-restack
=====================

Install with pip install git-restack

For assistance installing pip on your os check out get-pip:
http://pip.readthedocs.org/en/latest/installing.html

For installation from source simply add git-restack to your $PATH
after installing the dependencies listed in requirements.txt
