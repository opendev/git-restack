git-restack
===========

A git command for editing a series of commits without rebasing.

git-restack is a tool that performs an interactive git rebase of a
branch without changing the commit upon which the branch is based.

* Free software: Apache license
* Documentation: http://docs.openstack.org/infra/git-restack/
* Source: https://git.openstack.org/cgit/openstack-infra/git-restack
* Bugs: https://storyboard.openstack.org/#!/project/838
